require 'rails_helper'

describe Snack, type: :model do
  describe 'validations' do
    it { should validate_presence_of :title}
    it { should validate_presence_of :price}

    it { should have_many :machine_snacks }
    it {should have_many :machines }
  end
end
