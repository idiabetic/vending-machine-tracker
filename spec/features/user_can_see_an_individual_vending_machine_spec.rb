require 'rails_helper'

feature 'When a user visits a vending machine show page' do
  scenario 'they see the location of that machine' do
    owner = Owner.create(name: "Sam's Snacks")
    dons  = owner.machines.create(location: "Don's Mixed Drinks")
    item = dons.snacks.create!(title: 'Beer', price: 3.00)
    dons.snacks.create!(title: 'Beer', price: 3.00)

    visit machine_path(dons)

    expect(page).to have_content("Don's Mixed Drinks Vending Machine")
    expect(page).to have_content("Average Price: $3.00")
    expect(page).to have_content(item.title)
    expect(page).to have_content("$#{item.price}")
  end
end
