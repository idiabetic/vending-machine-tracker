require 'rails_helper'

feature "When I visit a snack show page" do
  scenario 'I see snack details' do
    owner = Owner.create(name: "Sam's Snacks")
    dons  = owner.machines.create(location: "Don's Mixed Drinks")
    item = dons.snacks.create!(title: 'Beer', price: 3.00)
    item = dons.snacks.create!(title: 'Beer', price: 3.00)

    visit snack_path(item)

    expect(page).to have_content(item.title)
    expect(page).to have_content("Snack Price: $#{item.price}")
    expect(page).to have_content(dons.location)
    expect(page).to have_content(dons.snacks.average(:price))
    expect(page).to have_content(dons.snacks.count)

  end
end
